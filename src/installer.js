var async = require('async');


/**
 * @constructor
 */
var Installer = function (config, readline) {
  this.$config = config;
  this.$readline = readline;

  this.registries_ = {};
  this.config_ = null;
};


Installer.prototype.setRegistries = function (registries) {
  this.registries_ = registries;
};


Installer.prototype.run = function (callback) {
  var installRegistry = this.installRegistry_.bind(this);

  var self = this;
  this.$config.load(function (err, config) {
    if (err) {
      callback(err);
    }

    self.config_ = config;

    var urls = Object.keys(self.registries_);
    async.forEach(urls, installRegistry, callback);
  });
};


Installer.prototype.installRegistry_ = function (url, callback) {
  var config = this.config_;

  var options = this.registries_[url];
  var scopes = this.listScopes_(options);
  if (scopes.length === 0) {
    return callback(null);
  }

  config.setCredentialsByURI(url, {
    username: this.$readline.question('Username: '),
    password: this.$readline.question('Password: ', { noEchoBack: true }),
    email: this.$readline.question('E-mail: '),
    alwaysAuth: Boolean(options['auth'])
  });

  scopes.forEach(function (scope) {
    config.del(scope + ':registry', 'user');
  });
  config.save('user', function (err) {
    if (err) {
      return callback(err);
    }

    scopes.forEach(function (scope) {
      config.set(scope + ':registry', url, 'user');
    });

    config.save('user', callback);
  });
};


Installer.prototype.listScopes_ = function (options) {
  var scopes = options['scopes'] || [];
  if (options['scope']) {
    scopes.push(options['scope']);
  }

  return scopes;
};


module.exports = Installer;
