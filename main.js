var path = require('path');

var Installer = require('./src/installer');


var main = function (args, fs, npmconf, readline, callback) {
  fs = fs || require('fs');
  npmconf = npmconf || require('npmconf');
  readline = readline || require('readline-sync');

  var filename = args[2] || path.join(process.env['HOME'], '.npm-registries.json');
  var registries = JSON.parse(fs.readFileSync(filename, 'utf8'));

  var installer = new Installer(npmconf, readline);
  installer.setRegistries(registries);

  installer.run(callback);
};


if (require.main === module) {
  main(process.argv, null, null, null, function (err) {
    process.exit(err ? 1 : 0);
  });
} else {
  exports.main = main;
}
